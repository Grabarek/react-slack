import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';


//Firebase configuration
var firebaseConfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: "react-slack-51dc1.firebaseapp.com",
    databaseURL: "https://react-slack-51dc1.firebaseio.com",
    projectId: "react-slack-51dc1",
    storageBucket: "react-slack-51dc1.appspot.com",
    messagingSenderId: "623074729639",
    appId: "1:623074729639:web:38c7437530528671"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);


export default firebase;
